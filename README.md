Join GNU/Linux client to Active Directory or Active Directory with Samba4
===

This script is based on this tutorial: 
    https://www.tecmint.com/join-ubuntu-to-active-directory-domain-member-samba-winbind/

## Tested on these distros

- Debian GNU/Linux Stretch
- Linux Mint 19 Tessa
- Ubuntu 16.04 & 18.04
